from __future__ import unicode_literals

from django.conf import settings

from rest_framework import serializers

class EmploymentSerializer(serializers.Serializer):
    title = serializers.CharField(required=False, allow_null=True, allow_blank=True)  # raw title
    title_norm = serializers.CharField(required=False, allow_null=True, allow_blank=True)
    seniority = serializers.CharField(required=False, allow_null=True, allow_blank=True)
    org = serializers.CharField(required=False, allow_null=True, allow_blank=True)  # raw company
    company_id = serializers.CharField(required=False, allow_null=True, allow_blank=True)
    start = serializers.CharField(required=False, allow_null=True, allow_blank=True)
    end = serializers.CharField(required=False, allow_null=True, allow_blank=True)
    description = serializers.CharField(required=False, allow_null=True, allow_blank=True)


class EducationSerializer(serializers.Serializer):
    qualification_type = serializers.CharField(required=False, allow_null=True, allow_blank=True)
    institution = serializers.CharField(required=False, allow_null=True, allow_blank=True)
    start = serializers.CharField(required=False, allow_null=True, allow_blank=True)
    end = serializers.CharField(required=False, allow_null=True, allow_blank=True)
    description = serializers.CharField(required=False, allow_null=True, allow_blank=True)

class WrittenCommunicationSerializer(serializers.Serializer):
    first_name = serializers.CharField(required=False, max_length=100, allow_null=True, default=None)
    last_name = serializers.CharField(required=False, max_length=100, allow_null=True, default=None)
    country = serializers.CharField(required=False,max_length=2,allow_null=True,default='uk')
    postal_code = serializers.CharField(required=False,max_length=20,allow_null=True,default=None)
    region  = serializers.CharField(required=False, max_length=100, allow_null=True, default=None)
    municipality = serializers.CharField(required=False, max_length=100, allow_null=True, default=None)
    address_line = serializers.CharField(required=False, allow_null=True, default=None)
    cv_email = serializers.CharField(required=False, max_length=100, allow_null=True, default=None)
    phone = serializers.CharField(required=False, max_length=100, allow_null=True, default=None)
    num_web_addresses = serializers.IntegerField(required=False, allow_null=True, default=None)
    num_referees = serializers.IntegerField(required=False, allow_null=True, default=None)
    cv_language = serializers.CharField(required=False, max_length=100, allow_null=True, default=None)
    raw_text =  serializers.CharField(required=False)
    work_history = EmploymentSerializer(required=False, many=True, default=[])
    education_history = EducationSerializer(required=False, many=True, default=[])
    months_work_exp = serializers.IntegerField(required=False, allow_null=True, default=None)
    months_management_exp = serializers.IntegerField(required=False, allow_null=True, default=None)
    