# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.http import HttpResponse
import json

from cv_scoring.CV_scoring import CV_scoring
# Create your views here.
from django.conf import settings
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from .serializers import WrittenCommunicationSerializer
"""
def index(request,data):


    
    data = json.loads(data)

    input_ =  {'first_name':data['first_name'],'last_name':data['last_name'],'country':data['country_code'],
                'postal_code':data['postal_code'],'region':data['region'],'municipality':data['municipality'],'address_line':data['address_line'],
                'cv_email':data['cv_email'],'phone':data['phone'],'raw_text':data['raw_text'],'months_work_exp':data['months_work_exp'],
                'months_management_exp':data['months_management_exp'],'work_history':data['work_history'],'education_history':data['education_history']}

    score = CV_scoring(input_).getQualityScore()
    
    return HttpResponse(
            json.dumps( {
            "spelling_errors_score": score['spelling_errors_score'],
            "bullets_homogeneity_score": score['bullets_homogeneity_score'],
            "good_terms_score": score['good_terms_score'],
            "length_score":score["length_score"],
            "pronouns_score":score['pronouns_score'],
            "overall_score":score['overall_score']
            })

    )
"""
class WrittenCommunication(APIView):

        def post(self,request):

                serializer = WrittenCommunicationSerializer(data=request.data)
                serializer.is_valid(raise_exception=True)
                data_input = serializer.validated_data

                score = CV_scoring(data_input).getQualityScore()

                return Response(score)