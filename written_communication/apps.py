from django.apps import AppConfig


class WrittenCommunicationConfig(AppConfig):
    name = 'written_communication'
