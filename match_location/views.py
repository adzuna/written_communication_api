
import json

from django.shortcuts import render
from django.http import HttpResponse
from django.conf import settings
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from .serializers import MatchLocationSerializer
import location_matcher.matcher


location_matchers = {
    "uk": location_matcher.matcher.LocationMatcher("uk"),
    "us": location_matcher.matcher.LocationMatcher("us"),
    "au": location_matcher.matcher.LocationMatcher("au"),
    "de": location_matcher.matcher.LocationMatcher("de"),
}


class MatchLocation(APIView):
    def post(self, request):
        serializer = MatchLocationSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        municipality = serializer.validated_data.get("municipality", "")
        postcode = serializer.validated_data.get("postcode", "")
        country = serializer.validated_data["country"]
        matcher = location_matchers[country.lower()]
        lat, lng, location_id = matcher.match_location(
            postcode=postcode if postcode else "",
            municipality=municipality if municipality else ""
            )
        location = {"lat": lat, "lng": lng, "location_id": location_id}
        return Response(location)
