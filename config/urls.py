"""apis URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf.urls import url
import written_communication.views
import match_location.views
import core.views


urlpatterns = [
    url(r'^written_communication/',written_communication.views.WrittenCommunication.as_view(),name="cv-quality-score"),
    url(r'^match_location/', match_location.views.MatchLocation.as_view(), name ="match-location"),
    url(r'^_health/',core.views.HealthCheckView.as_view(),name='health')
]
