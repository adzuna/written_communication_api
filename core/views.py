from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView


class HealthCheckView(APIView):

    def get(self, request):

        all_status = {"status": "up"}

        if "down" in all_status.values():
            return Response(data=all_status, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response(data=all_status, status=status.HTTP_200_OK)



