import json
import os
from django.conf import settings
from rest_framework.test import APITestCase


class WrittenCommunicationTest(APITestCase):

    def setUp(self):

        self.written_communication_url = '/written_communication/'

    def test_api(self):

        response = self.client.post(self.written_communication_url,data={"first_name":"John","last_name":"Doe","raw_text":"sdfdsfds","months_work_exp": 120,"months_management_exp": 36})
        
        self.assertEqual(response.status_code,200)

    