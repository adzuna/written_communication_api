# Written Communication API Backend

CV scoring package deployed as a Django REST API in AWS Elastic Beanstalk. More details about the CV scoring package you can find [here](https://bitbucket.org/adzuna/cv_scoring/src/master/).


## Content

* `config`: django config: settings, url and wsgi app.
* `written_communication` : django apps
* `core` health check page
* `wheelhouse` : python packages
* `API.md` : documentation
* `requirements/` : python dependencies.
* `Makefile` : some shortcuts to setup the dev environment, run django dev server etc...

## Project Setup for Development

 
* Clone the repository
* Install python dependencies with ``make install``. 
This will create a virtual environment in `~/.virtualenvs/written_communication` and install all dev dependencies (from `wheelhouse/`).
* Run tests with ``make test``

## Start django development server

```bash
make runserver
```

## Example

```python

import json
import os
import hrxml_parser.parser
import requests

parsed_cv = hrxml_parser.parser.parse_hr_xml(open(filename,'r').read())

url_written_communication = "http://written-communication-api-dev.us-east-1.elasticbeanstalk.com/written_communication/"

response = requests.post(url_written_communication,json=parsed_cv)
print(response.text)
```


# Contact 

manolis@adzuna.com

