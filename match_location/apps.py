from django.apps import AppConfig


class LocationMatcherConfig(AppConfig):
    name = 'location_matcher'
