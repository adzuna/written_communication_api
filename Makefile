PROJECT_DIR := $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))
PROJECT_NAME = written_communication
PYTHON_INTERPRETER = python3.6
VENV_LOCATION = ~/.virtualenvs/written_communication

#################################################################################
# COMMANDS                                                                      #
#################################################################################

## Setup dev environment (includes dev dependencies)
install:
	rm -rf $(VENV_LOCATION); \
	virtualenv -p $(PYTHON_INTERPRETER) --no-site-packages $(VENV_LOCATION); \
	$(VENV_LOCATION)/bin/pip install --no-index --find-links=$(PROJECT_DIR)/wheelhouse/ -r requirements.txt; \
	echo "Done !"

test:
	. $(VENV_LOCATION)/bin/activate; \
        export DJANGO_SETTINGS_MODULE=config.settings; \
        $(PROJECT_DIR)/manage.py test test



## Run Django dev server:
runserver:
	. $(VENV_LOCATION)/bin/activate; \
	export DJANGO_SETTINGS_MODULE=config.settings; \
	$(PROJECT_DIR)/manage.py runserver

