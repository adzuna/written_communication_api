from __future__ import unicode_literals

from django.conf import settings

from rest_framework import serializers


class MatchLocationSerializer(serializers.Serializer):
    municipality = serializers.CharField(required=False, allow_null=True, allow_blank=True)
    postcode = serializers.CharField(required=False, allow_null=True, allow_blank=True)
    country = serializers.CharField(required=True, allow_null=False, allow_blank=False)
